//useState es para usar ReactHooks
import React, { Fragment, useState } from "react";

const InputTodo = () => {
  // constante con el estado y el cambio y useState vacío/hello
  const [description, setDescription] = useState("");

  const onSubmitForm = async e => {
    e.preventDefault();
    try {
      //creo constante body para recuperar description
      const body = { description }
      //constante response para hacer una petición al backend y enviar datos
      const response = await fetch("http://localhost:5000/todos", {
        method: "POST", //método http a usar
        headers: { "Content-Type": "application/json" }, //cabeceras de seguridad
        body: JSON.stringify(body) //el cuerpo a enviar
      });

      window.location ='/'; //refresca redireccionando... no me gusta pero bueno...
      console.log(response); //consoleamos resultado
      
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      <h1 className="text-center mt-5">Pern Todo List</h1>
      {/* conectamos con envío del formulario */}
      <form className="d-flex mt-5" onSubmit={onSubmitForm}>
        {/* recogemos la variable description */}
        <input type="text" className="form-control" value={description}
          // ejecutamos setDescription cuando el cambio del input se produzca
          onChange={e => setDescription(e.target.value)}
        />
        <button className="btn btn-success">Add</button>
      </form>
    </Fragment>
  )
}
export default InputTodo;