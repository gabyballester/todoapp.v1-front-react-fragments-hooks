// Importamos react, fragments y useState
import React, { Fragment, useState } from "react";

const EditTodo = ({ todo }) => { //recuperamos todo
  // seteamos el estado por defecto todo.description
  const [description, setDescription] = useState(todo.description);

  // agregamos función para actualizar descripción
  const updateDescription = async e => {
    e.preventDefault();
    try { // creamos variable body con la descripción
      const body = { description };
      const response = await fetch( //recogemos la respuesta de la api
        `http://localhost:5000/todos/${todo.todo_id}`, //ruta e id a actualizar
        { // le pasamos objeto con metodo, cabeceras y el body stringifeado
          method: "PUT",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(body)
        }
      );
      window.location = "/";
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      {/* Trigger the modal with a button */}
      <button type="button" className="btn btn-warning"
        data-toggle="modal"
        data-target={`#id${todo.todo_id}`}
      > Edit </button>

      {/* Modal */}
      <div
        className="modal fade"
        id={`id${todo.todo_id}`}
        onClick={() => setDescription(todo.description)}
      >
        <div className="modal-dialog">
          {/* Modal content */}
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Edit Todo</h4>
              {/* botón de la x para cerrar ventana */}
              <button type="button" className="close"
                data-dismiss="modal"
                onClick={() => setDescription(todo.description)}
              >&times;</button>
            </div>

            <div className="modal-body">
              <input type="text" className="form-control"
                value={description}
                /** configuramos el onchange para que ejecute la función que setea
                el value con lo que haya en el input en ese momento */
                onChange={e => setDescription(e.target.value)}
              />
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-warning"
                data-dismiss="modal"
                /** Implementamos llamada a función que actualiza descripción */
                onClick={e => updateDescription(e)}
              > Save </button>

              <button type="button" className="btn btn-danger"
                data-dismiss="modal"
                onClick={() => setDescription(todo.description)}
              > Close </button>

            </div>
          </div>

        </div>
      </div>
    </Fragment>
  )
}

export default EditTodo;
