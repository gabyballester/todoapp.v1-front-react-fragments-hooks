import React, { Fragment, useEffect, useState } from "react";
import EditTodo from './EditTodo'; //importamos componente

const ListTodos = () => {

    // constante con el estado y el cambio y useState vacío
    const [todos, setTodos] = useState([]); // array vacío por defecto

    // método para el borrado
    const deleteTodo = async (id) => { //pasamos el id a borrar
        try {
            //hacemos llamada a la api pasándole el id
            //(template strings permite ejecutar javascript)
            const deleteTodo = await fetch(`http://localhost:5000/todos/${id}`, {
                method: "DELETE"
            });

            setTodos(todos.filter(todo => todo.todo_id !== id));

        } catch (err) {
            console.error(err.message);
        }
    };

    // método para recuperarlos todos
    const getTodos = async () => { //definimos función getTodos
        try {                //fetch hace una petición GET por defecto
            const response = await fetch("http://localhost:5000/todos");
            const jsonData = await response.json(); //parseamos la respuesta

            setTodos(jsonData); //seteamos el estado con lo que le paso por parámetro
        } catch (err) {
            console.error(err.message);
        }
    };

    useEffect(() => { // callback con petición fetch cuando componente renderiza
        getTodos();
    }, []);

    console.log(todos);

    return (
        <Fragment>
            {" "}
            <table className="table mt-5 text-center">
                <thead>
                    <tr>
                        <th>Description</th>
                        {/* botones edit y delete */}
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {/*<tr>
                    <td>John</td>
                    <td>Doe</td>
                    <td>john@example.com</td>
                    </tr>*/}
                    {todos.map(todo => (
                        <tr key={todo.todo_id}>
                            <td>{todo.description}</td>
                            <td> <EditTodo todo={todo} /></td>
                            <td> <button
                                className="btn btn-danger"
                                // Vento que llama a deleteTodo y le pasa el unique key
                                onClick={() => deleteTodo(todo.todo_id)}
                            > Delete </button> </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </Fragment>
    )
}

export default ListTodos;